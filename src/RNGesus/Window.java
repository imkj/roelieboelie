package RNGesus;

import java.awt.*;
import java.awt.event.ActionEvent;
import javax.swing.*;
import java.util.Random;

public class Window extends JFrame {

    Dampe _dampe;
    JPanel pnlButton = new JPanel();
    JPanel pnlText = new JPanel();
    JButton dig = new JButton("Dig! (10 Rupees)");
    JButton quit = new JButton("Quit");
    JLabel money;
    JLabel label = new JLabel("Hello, I'm Dampè. Would you like to play a game?");

    public Window()
    {
        super("Dampè plz one time ResidentSleeper");
        setSize(750,500);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        _dampe = new Dampe();
        money = new JLabel("You have " + _dampe.getRupees().toString() + " Rupees.");

        Random random = new Random();
        int rand = random.nextInt(10) + 1;

        switch(rand)
        {
            case 1:
                LabelUpdate("Heart piece");
                System.out.println("Heart Piece");
                break;

            case 2:
                LabelUpdate("You earn 10 Rupees.");
                System.out.println("10 rupees");
                break;

            case 3:
                LabelUpdate("You earn 20 Rupees.");
                System.out.println("20 rupees");
                break;

            default:
                LabelUpdate("You get nothing!");
                System.out.println("You get nothing!");
        }

        quit.addActionListener((ActionEvent e) -> System.exit(0)); //this is equivalent to:
        /*
            quit.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    System.exit(0);
                }
            });
        */

        dig.addActionListener((ActionEvent e) -> {
            if(_dampe.getRupees() >= 10) {
                _dampe.setRupees(_dampe.getRupees() - 10);
                String rupeesString = _dampe.getRupees().toString();
                money.setText("You have " + rupeesString + " Rupees.");
            } else {
                label.setText("You don't have enough rupees!");
            }
        });
        /*
            Again, equivalent to:
            dig.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    ...
                }
            });
         */

        add(pnlText, BorderLayout.CENTER);
        add(pnlButton, BorderLayout.SOUTH);

        pnlButton.add(money);
        pnlButton.add(dig);
        pnlButton.add(quit);
        pnlText.add(label);

        setVisible(true);
        setResizable(false);
    }


    //Here's the important bit.
    public void LabelUpdate(String newLabel)
    {
        label.setText(newLabel);
    }
}

